from django.db import models
from django.urls import reverse


class Technicians(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False, null=True)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField(max_length=1000)
    status = models.TextField(max_length=200, default="Created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    vip = models.BooleanField(default=False, null=True, blank=True)
    technician = models.ForeignKey(
        Technicians,
        related_name='appointment',
        on_delete=models.CASCADE
    )

    def finished(self):
        self.status = "Finished"
        self.save()

    def cancelled(self):
        self.status = "Cancelled"
        self.save()

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
