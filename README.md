# CarCar

Team:

* Person 1 - Nick - Sales
* Person 2 - Marcus - Services
## Getting Started

**Make sure you have Docker, Git, and Node.js 18.2 or above**

1. Fork this repository  <https://gitlab.com/nickdlf/project-beta.git>

2. Clone the forked repository into a directory on your local computer:
git clone <https://gitlab.com/nickdlf/project-beta.git>

3. From a terminal or CLI, change directory. Ensure you

4. From a terminal or CLI, build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running
- View the project in the browser: http://localhost:3000/. It should appear like image below




## Design

![Img](/images/CarCarWebsite.png)

CarCar is made up of 3 microservices which interact with one another. Both sales and services implement a poller that maintains up to date information on the status of a vehicle (either sold or unsold) and tracked by a specific vin. This vin is later used in other models, such as Sale to maintain the status of the vehicle.

- **Inventory** - This hosts all the inventory of each vehicle, both sold and unsold. This details the make, model and VIN of each vehicle. This microservice houses three models.

    - **Manufacturer** :
        - *name* - a Charfield attribute for the specific name of the manufacturer
        - Manufacturer also returns a URL based on a primary key.

    - **VehicleModel** :
        - *Name* - a CharField attribute of rthe specific name of vehicle model
        - *picture_url* - a URLfrield for a specific picture
        - *manufacturer* - foreign key attribute that is specifying many VehicleModel instances to one instance of a manufacturer. If the manufacturer is deleted, then the VehicleModel Model class will also be deleted. A VehicleModel instance cannot exist without a manufacturer.
        - *VehicleModel* also returns a url based on a primary key.

    - **Automobile**
        - *color* - Charfield for color.
        - *year* - PositiveSmallIntegerField for year.
        - *vin* - charfield, max length of 17 and must be unique.
        - *sold* - BooleanField with default set to false. Default of false is important because it allows the inventory model to populate the sales data with unsold vehicles.
        - *model* - foreign key attribute that is specifying many Automobile instances to one instance of a VehicleModel. If the model is deleted, then the Automobile class will also be deleted. An Automobile instance cannot exist without a VehicleModel.
        - Automobile also returns a url based on a vin.

- **Services** -  The Services microservice enables customers to schedule appointments for their vehicles. It communicates with the Inventory microservice to acquire data about vehicles in the inventory, both sold and unsold. Two-way communication is enabled between these microservices, facilitating updates in the appointment statuses in real-time. When an appointment is created for a particular vehicle within the inventory, the 'appointment_status' is updated to 'Scheduled'. The microservice keeps track of all service appointments, including the customer, the vehicle involved, the type of service requested, and the assigned technician. It plays a crucial role in managing the after-sales services provided by CarCar, ensuring customer satisfaction and efficient service scheduling.

    - **AutomobileVO**
        - *vin* - Charfield, unique, max lenght 17. This is information is polled from the seperate inventory model. The Automobile VO is a value object that represents a reference of the Automobile model in the inventory. Only the necessary information is polled using the poller. In this microservive we only lookup the vin of the automobile and it's sold status using a boolean.
        - *sold* - Boolean field with default set to false. This is used in our sales API to update the status of a vehicle from our inventory microservice.

    - **Technician** :
        - *employee_id* - Charfield and unique identifier for the employee ID. In our POSTs for different models we look up each salesperson by their unique employee ID.
        - *first_name* - Charfield, employee first name.
        - *last_name* -  Charfield, employee last name.

    - **Appointment** :
        - *date_time* - Datetime field for when an appointment is scheduled.
        - *reason* - Text field for making the appointment, max length 1000.
        - *status* -  Text field with default of "Created"
        - *vin* -  Vin, max length 17.
        - *customer* -  Charfield max length.
        - *vip* -  Default of false, checks if a customer is a VIP based on if the associated VIN has a sold status of "true" from our inventory microservice.
        - *technician* -  Foreign key. Many instances of an appointment to on technician.

- **Sales** - The Sales microservice allows the staff to see which vehicles remain unsold within the inventory as well as view a list of sold and unsold models. The sales microservice polls the inventory microservice for an up to date list of the available inventory. Two way communication has been enabled between the two microservices to allow frontend interaction to update changes to the inventory. Specifically, when a sale is made on a particular vehicle within the inventory, the status of the sale is update to 'True'.

    - **Salesperson** :
        - *employee_id* - Charfield and unique identifier for the employee ID. In our POSTs for different models we look up each salesperson by their unique employee ID.
        - *first_name* - Charfield, employee first name.
        - *last_name* -  Charfield, employee last name.

    - **Customer** :
        - *first_name* - Charfield, customer first name.
        - *last_name* - Charfield, customer last name.
        - *address* - Charfield, customer address field.
        - *phone_number* - Charfield, unique, max length of 17, Charfield. In our POSTs for different models we look up each employee by their unique phone numbers.

    - **AutomobileVO**
        - *vin* - Charfield, unique, max lenght 17. This is information is polled from the seperate inventory model. The Automobile VO is a value object that represents a reference of the Automobile model in the inventory. Only the necessary information is polled using the poller. In this microservive we only lookup the vin of the automobile and it's sold status using a boolean.
        - *sold* - Boolean field with default set to false. This is used in our sales API to update the status of a vehicle from our inventory microservice.


    - **Sale**
        - *price* - Decimal field, max digits of 10 and two decimal places. This is the price of the sale of the car.
        - *automobile* - foreign key attribute that is specifying many sale instances to one instance of an automobile. This automobile is referencing the AutomobileVO model, which means it will contain only the sold status as well as the vin number.  An instance of a sale cannot exist without an AutomobileVO instance.
        - *salesperson* - foreign key attribute that is specifying many sale instances to one instance of a salesperson.  An instance of a sale cannot exist without a Salesperson. Salespersons cannot be deleted if they are associated with an instance of a sale.
        - *customer* - foreign key attribute that is specifying many sale instances to one instance of a customer.  An instance of a sale cannot exist without a customer. Customers cannot be deleted if they are associated with an instance of a sale.


![Img](/images/CarCarDiagram.png)


## Accessing Endpoints to Send and View Data: Access Through Insomnia & Your Browser

### Manufacturers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/


JSON body to send data:

Create and Update a manufacturer (SEND THIS JSON BODY):
- You cannot make two manufacturers with the same name
```
{
  "name": "Chrysler"
}
```
- The return value of creating, viewing, updating a single manufacturer:
```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```
- Getting a list of manufacturers return value:
```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

### Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

- Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

- Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```
- Return value of creating or updating a vehicle model:
- This returns the manufacturer's information as well
```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
- Getting a List of Vehicle Models Return Value:
```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

### Automobiles:
- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/


Create an automobile (SEND THIS JSON BODY):
- You cannot make two automobiles with the same vin
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
- Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```
To get the details of a specific automobile, you can query by its VIN:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

- Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```
- You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```
- Getting a list of Automobile Return Value:
```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```
# Sales Microservice

On the backend, the sales microservice has 4 models: AutomobileVO, Customer, Salesperson, and Sale. Customer, Salesperson, AutomobileVO are all foreign keys on the Sale model. One of the implications of all these being a foreign key is that an instance of a Sale could not exist without the other three models existing. This makes sense as this is typically true and mirrors how the sales process and a sale occurs in the physical world.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data. The poller is only concerned with getting only the necessary information. On the sales microservice, the poller gets the sold status and the vin of the vehicle. Whether or not the vehicle was sold is an important distinction on the CarCar application as it's later used on our frontend logic to filter vehicles on whether or not it has been sold. The prevents the staff from having to see unecessary information, such as vehicles that do not exist in the inventory.

## Accessing Endpoints to Send and View Data - Access through Insomnia:

### Customers:


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | POST | http://localhost:8090/api/customers/
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/id/


###### Create a new customer - POST - http://localhost:8090/api/customers/
- Note, each customer *must* have a unique phone number:
```
{
	  "first_name": "Bart",
    "last_name": "Harley-Jarvis",
    "address": "1428 Elm Street",
    "phone_number": "2813308004"

}
```
- Return Value of Creating a Customer:

```
{
	"id": 10,
	"first_name": "Bart",
	"last_name": "Harley-Jarvis",
	"address": "1428 Elm Street",
	"phone_number": "2813308004"
}
```
###### Get list of all customers - GET - http://localhost:8090/api/customers/
```
{
	"customers": [
		{
			"id": 10,
			"first_name": "Bart",
			"last_name": "Harley-Jarvis",
			"address": "1428 Elm Street",
			"phone_number": "2813308004"
		},
		{
			"id": 11,
			"first_name": "Taffy",
			"last_name": "Lee-Fubbins",
			"address": "1600 Penn Ave",
			"phone_number": "8675309"
		}
	]
}
```

###### Delete a customer - DELETE -http://localhost:8090/api/customers/id/
- Note, attempted deletion of a customer associated with another instance of a foreign key will result in a protected error.

```
{
	"message": "customer deleted successfully"
}
 OR

{
	"message": "customer does not exist"
}
```


### Salespeople:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/id/



###### Create a new salesperson - POST - http://localhost:8090/api/salespeople/
- Note, each customer *must* have a unique employee ID:
```
{
    "employee_id": "jFenton",
    "first_name": "Jake",
    "last_name": "Fenton"
}
```
- Return Value of creating a salesperson:
```
{
	"id": 28,
	"employee_id": "jFenton",
	"first_name": "Jake",
	"last_name": "Fenton"
}
```
- List all salespeople Return Value:

###### Get list of all salespeoeple - GET -http://localhost:8090/api/salespeople/
```
{
	"salespeople": [
		{
			"id": 24,
			"employee_id": "PAULb",
			"first_name": "Paul",
			"last_name": "Bunyon"
		},
		{
			"id": 28,
			"employee_id": "jFenton",
			"first_name": "Jake",
			"last_name": "Fenton"
		}
	]
}
```

###### DELETE -http://localhost:8090/api/salespeople/id/
- Attempted deletion of a salesperson associated with another instance of a foreign key will result in a protected error.
```

{
	"message": "Salesperson deleted successfully"
}
 OR

{
	"message": "Salesperson does not exist"
}

```


### Sale:
- Each sale has its own ID with encoder that show all the data for entire transaction: automobile, salesperson, customer.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Delete a sale | DELETE | http://localhost:8090/api/sales/id/


###### Get list of all sales - GET -  http://localhost:8090/api/sales/
- List all sales of the dealership.
```

	"sales": [
		{
			"id": 22,
			"price": "3000.00",
			"automobile": {
				"id": 7,
				"vin": "HSG7",
				"sold": true
			},
			"salesperson": {
				"id": 23,
				"employee_id": "21",
				"first_name": "John",
				"last_name": "Smith"
			},
			"customer": {
				"id": 5,
				"first_name": "Customer 4",
				"last_name": "Customer 4",
				"address": "4 Bob Lane",
				"phone_number": "111111"
			}
		},
		{
			"id": 23,
			"price": "4000.00",
			"automobile": {
				"id": 13,
				"vin": "KIASORRENTOVIN",
				"sold": true
			},
			"salesperson": {
				"id": 24,
				"employee_id": "PAULb",
				"first_name": "Paul",
				"last_name": "Bunyon"
			},
			"customer": {
				"id": 8,
				"first_name": "Tom",
				"last_name": "Jerry",
				"address": "222 Apple Lane",
				"phone_number": "7896513456"
			}
		},
```
###### Create a New Sale - POST -  http://localhost:8090/api/sales/
- Note, VIN, Automobile and Customer MUST exist for a sale to be made. Must include, automobile vin, employee ID and customer phone number to properly record Sale transaction. What's returned is all of the data for the sale.
```
{

	"price": 4999.99,
	"automobile": "4S3BK6354S6355265",
	"salesperson": "jFenton",
	"customer": "8675309"

}
```
- Return Value of Creating a New Sale:
```
{
	"id": 26,
	"price": 4999.99,
	"automobile": {
		"id": 15,
		"vin": "4S3BK6354S6355265",
		"sold": true
	},
	"salesperson": {
		"id": 28,
		"employee_id": "jFenton",
		"first_name": "Jake",
		"last_name": "Fenton"
	},
	"customer": {
		"id": 11,
		"first_name": "Taffy",
		"last_name": "Lee-Fubbins",
		"address": "1600 Penn Ave",
		"phone_number": "8675309"
	}
}

```
###### Delete a Sale - DELETE -  http://localhost:8090/api/sales/id/
- Note, sales records can be deleted easily and are not protected, despite sharing associations with many other models.
```
{
	"message": "sale deleted successfully"
}

{
	"message": "sale does not exist"
}
```

# Service microservice

I created 3 models Technicians, AutomobileVO and Appointment. The Technicians model shows the properties first name, last name, and employee id. This is mainly used to create a form and a list for our services microservice to show who is available to work on the car. The Appointment model has the properties date time, reason, status, vin, customer, vip, and technician. Technician is a foreign key to the appointment model so that it ensures that you cannot make an appointment without a technician. The automobileVO is used to connect the automobile microservice to the services microservice. It is connected so that I can use it to filter for the VIN when filtering what is in our inventory and is used to compare against was has been sold to see who is a VIP or not.

The main components of service are as follows:
1. Our friendly technician staff
2. Service Appointments


### Technicians - The people of CarCar


| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:pk>/


LIST TECHNICIANS: Following this endpoint will give you a list of all technicians that are currently employed.
Since this is a GET request, you do not need to provide any data.
```
Example:
{

	"first_name": "Rey",
	"last_name": "Mysterio",
	"employee_id": "1",
	"id": 1

}
```
CREATE TECHNICIAN - What if we hired a new technician (In this economy even)? To create a technician, you would use the following format to input the data and you would just submit this as a POST request. Note, employee_id is a unique input.
```
{
	"first_name" : "Under",
  	"last_name" : "Taker",
  	"employee_id" : 7
}
```
As you can see, the data has the same format. In this example, we just changed the "name" field from "under taker" to "dave bautista". We also assigned him the "employee_id" value of "14" instead of "7".
Once we have the data into your request, we just hit "Send" and it will create the technician "dave bautista". To verify that it worked, just select follow the "LIST TECHNICIAN" step from above to show all technicians.
With any luck, both Rey and Dave will be there.
Here is what you should see if you select "LIST TECHNICIAN" after you "CREATE TECHNICIAN" with Under Taker added in:
```
{
	"technicians": [
		{
			"first_name": "Under",
			"last_name": "Taker",
			"employee_id": 7,
			"id": 7
		},
		{
			"first_name": "Dave",
			"last_name": "Bautista",
			"employee_id": 7,
			"id": 7
		},
```

DELETE TECHNICIAN - If we decide to "go another direction" as my first boss told me, then we need to remove the technician from the system. To do this, you just need to change the request type to "DELETE" instead of "POST". You also need to pull the "id" value just like you did in "TECHNICIAN DETAIL" to make sure you delete the correct one. Once they are "promoted to customer" they will no longer be in our page that lists all technicians.


And that's it! You can view all technicians, look at the details of each technician, and create technicians.
Remember, the "id" field is AUTOMATICALLY generated by the program. So you don't have to input that information. Just follow the steps in CREATE TECHNICIAN and the "id" field will be populated for you.
If you get an error, make sure your server is running and that you are feeding it in the data that it is requesting.
If you feed in the following:
```
{
	"first_name": "Under",
	"last_name": "Taker",
	"employee_number": 3,
	"favorite_food": "Tacos"
}

You will get an error because the system doesn't know what what to do with "Tacos" because we aren't ever asking for that data. We can only send in data that Json is expecting or else it will get angry at us.

```


### Appointments - The business aspect of what we do here at CarCar

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List appointment | GET | http://localhost:8080/api/appointments/
| Create a appointment | POST | http://localhost:8080/api/appointments/
| Delete a appointment | DELETE | http://localhost:8080/api/appointments/<int:pk>/
| Update a appointment | PUT | http://localhost:8080/api/appointments/<int:pk>/cancel
| Update a appointment | PUT | http://localhost:8080/api/appointments/<int:pk>/finish


LIST APPOINTMENT: Following this endpoint will give you a list of all appointments that are currently in the system.
Since this is a GET request, you do not need to provide any data.
```
Example:
{
	"date_time": "2023-06-28T15:05:00+00:00",
	"reason": "tires",
	"vip": false,
	"status": "Cancelled",
	"customer": "john jones",
	"vin": "1B",
	"technician": {
		"first_name": "Rey",
		"last_name": "Mysterio",
		"employee_id": "1",
		"id": 1
	},
	"id": 3
}
```

CREATE APPOINTMENT - What if we received a new appointment ? To create a appointment, you would use the following format to input the data and you would just submit this as a POST request. Note, id is a unique input.
```
{
	"date_time": "2023-07-19T12:03:00+00:00",
	"reason": "windows",
	"vip": false,
	"status": "Created",
	"customer": "john jones",
	"vin": "1C",
	"technician": 2,
	"id": 112
}
```

DELETE APPOINTMENT - If we decide to remove the APPOINTMENT from the system. To do this, you just need to change the request type to "DELETE" instead of "POST". You also need to pull the "id" value as we did for technician. We do not need to input a JSON body since we are only using the id to delete. When a appointment is deleted successfully it will show this message:
```
{
	"deleted": true
}
```

UPDATE APPOINTMENT - If we need to mark a appoint as Cancelled then we need to input a JSON body with the updated "status" to "Cancelled". Note that this is a keyword and must be spelled exactly like that. We will also need to input the specific id to the endpoint to target a specific appointment.
```
{
	"date_time": "2023-07-02T12:39:00+00:00",
	"reason": "tires",
	"vip": false,
	"status": "Cancelled",
	"customer": "john jones",
	"vin": "1B",
	"technician": 1,
	"id": 2
}
```

UPDATE APPOINTMENT - If we need to mark a appoint as Finished then we need to input a JSON body with the updated "status" to "Finished". Note that this is a keyword and must be spelled exactly like that. We will also need to input the specific id to the endpoint to target a specific appointment.
```
{
	"date_time": "2023-07-02T12:39:00+00:00",
	"reason": "tires",
	"vip": false,
	"status": "Finished",
	"customer": "john jones",
	"vin": "1B",
	"technician": 1,
	"id": 2
}
```
