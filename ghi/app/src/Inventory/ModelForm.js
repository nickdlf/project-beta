import { useState } from 'react';


function ModelForm({ onAddModel, manufacturers }) {
    const [name, setName] = useState('');
    const [picture_url, setPictureURL] = useState('');
    const [manufacturer_id, setManuID] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            name,
            picture_url,
            manufacturer_id,
        };

        const response = await fetch("http://localhost:8100/api/models/", {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (response.ok) {
            alert('Model added successfully!');
            setName('');
            setPictureURL('');
            setManuID('');
            onAddModel();
        } else {
            alert('An error occurred while adding model.');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new model</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={(e) => setName(e.target.value)} placeholder="Model Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={picture_url} onChange={(e) => setPictureURL(e.target.value)} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select value={manufacturer_id} onChange={(e) => setManuID(e.target.value)} className="form-control">
                                <option value=""> Select Manufacturer </option>
                                {manufacturers.map(manufacturer => <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>)}
                            </select>
                            <label htmlFor="manufacturer_id"> Manufacturer </label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ModelForm;
