import React, { useState, useEffect } from 'react';

function AppointmentHistory() {
  const [appointments, setAppointments] = useState([]);
  const [vin, setVin] = useState('');
  const [automobiles, setAutomobiles] = useState([]);



  async function loadAppointments() {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointment);
   
    } else {
      
    }
  }


  function filterAppointments() {

    if (vin === ''){
      setVin('')
      loadAppointments();
    }

    const filteredAppointments = appointments.filter(appointment => appointment.vin === vin);
    setAppointments(filteredAppointments);
  }

  function handleVinChange(event) {
    setVin(event.target.value);
  }


  async function fetchSales() {
    const salesurl = 'http://localhost:8090/api/sales/';
    const response = await fetch(salesurl);
    if (response.ok) {
      const salesdata = await response.json();
      setAutomobiles(salesdata.sales);

    }
  }

  useEffect(() => {
    loadAppointments();
    fetchSales();
  }, []);

  return (
    <div>
      <h1 style={{ marginTop: '10px' }}>Service History</h1>
      <div className="form-floating mb-3">
        <input
          value={vin}
          onChange={handleVinChange}
          placeholder="Vin"
          required
          type="text"
          name="vin"
          id="vin"
          className="form-control"
        />
        <label htmlFor="vin">Search by VIN</label>
        <button onClick={() => filterAppointments()} className="btn btn-primary">
          Search
        </button>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>

      {appointments.map((appointment) => {
      const is_vip = automobiles.filter((auto) => {
        return "" || auto.automobile.vin === appointment.vin;}).length > 0;
      return (
        <tr key={appointment.id}>
          <td>{appointment.vin}</td>
          <td>{is_vip ? "Yes" : "No"}</td>
          <td>{appointment.customer}</td>
          <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
          <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
          <td>
            {appointment.technician.first_name + " " + appointment.technician.last_name}
          </td>
          <td>{appointment.reason}</td>
          <td>{appointment.status}</td>
        </tr>
      );
    })}
  </tbody>
  </table>
  </div>
  );
  }
  export default AppointmentHistory;
