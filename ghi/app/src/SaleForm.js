import { useState, useEffect } from 'react';

function SaleForm( {onAddSale}) {
  const [price, setPrice] = useState('')
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);

  const [salesperson, setSalesperson] = useState('');
  const [salespeople, setSalespeople] = useState([]);

  const [automobile, setAuto] = useState('');
  const [automobiles, setAutos] = useState([]);



  async function fetchCustomers() {
    const url = 'http://localhost:8090/api/customers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }

  async function fetchSalespeople() {
    const url = 'http://localhost:8090/api/salespeople/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  async function fetchAutos() {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  }


  useEffect(() => {
    fetchCustomers();
    fetchSalespeople();
    fetchAutos();
  }, [])


  const handleSubmit = async (e) => {
    e.preventDefault();
  
    const data = {
      price,
      automobile,
      salesperson,
      customer,
    };
   
    try {
      const responseSale = await fetch("http://localhost:8090/api/sales/", {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      });
  
      if (responseSale.ok) { // if fetch request from sale is good, PUT sold to backend --> status will be updated via poller
        const responseInventory = await fetch(`http://localhost:8100/api/automobiles/${automobile}/`, {
          method: 'PUT',
          body: JSON.stringify({ "sold": true }),
          headers: {
            'Content-Type': 'application/json'
          }
        });
  
        if (!responseInventory.ok) {
          alert(`Inventory update failed: ${responseInventory.status}`);
        } else {
          alert('Sale and inventory updated successfully!');
          fetchAutos(); 
          setAuto('');
          setSalesperson('');
          setCustomer('');
          setPrice('');
          onAddSale(); 
        }
      } else {
        alert('An error occurred while adding sale.');
      }
    } catch (error) {
      console.error(error);
      alert('An error occurred while processing your request. Please try again.');
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
            <select required value={automobile} onChange={(e)=> setAuto(e.target.value)} placeholder="Select Vehicle" type="text" name="auto" id="auto" className="form-control">
                <option value=""> Select Vehicle </option>
                {automobiles.filter(automobile=> !automobile.sold).map(automobile => {return (
                <option key={automobile.vin} value={automobile.vin}>{automobile.year} {automobile.color} {automobile.model.name} {automobile.vin}  </option>)})} 
            </select>
            <label htmlFor="vin"> Vehicle </label>
            </div>
            <div className="form-floating mb-3">
            <select value={salesperson} onChange={(e) => setSalesperson(e.target.value)} className="form-control">
                <option value=""> Select Salesperson </option>
                {salespeople.map(salesperson => <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>)}
            </select>
            <label htmlFor="salesperson"> Salesperson </label>
            </div>
            <div className="form-floating mb-3">
                <select value={customer} onChange={(e) => setCustomer(e.target.value)} className="form-control">
                    <option value=""> Select customer </option>
                    {customers.map(customer => <option key={customer.phone_number} value={customer.phone_number}>{customer.first_name} {customer.last_name}</option>)}
                </select>
                <label htmlFor="salesperson"> Customer </label>
            </div>
                <div className="form-floating mb-3">
                    <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} className="form-control" placeholder="Price" required />
                    <label htmlFor="price">Price</label>
                </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}


export default SaleForm;