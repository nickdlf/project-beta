function ListCustomers( {customers }) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
          <th>Phone Number</th>
          <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer => {
            return (
              <tr key={customer.phone_number}>
                <td>{ customer.first_name }</td>
                <td>{ customer.last_name }</td>
                <td>{ customer.phone_number }</td>
                <td>{ customer.address }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
  export default ListCustomers;
  