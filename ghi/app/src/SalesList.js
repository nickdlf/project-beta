function ListCustomers( { sales }) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
          <th>Employee ID</th>
          <th>Salesperson Name</th>
          <th>VIN</th>
          <th>Customer</th>
          <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{ sale.salesperson.employee_id }</td>
                <td>{ sale.salesperson.first_name } { sale.salesperson.last_name}</td>
                <td>{ sale.automobile.vin }</td>
                <td>{ sale.customer.first_name } { sale.customer.last_name}</td>
                <td>{ `$${sale.price}`}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
  
  export default ListCustomers;
  