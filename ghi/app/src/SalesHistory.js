import { useState, useEffect} from 'react';

function SaleHistory() {
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');

  const fetchSalesData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

  const fetchSalespeopleData = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }

  useEffect(() => {
    fetchSalesData();
    fetchSalespeopleData();
  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1 className="text-4xl font-bold text-black">Sales History by Salesperson</h1>
          <select className="form-control" 
            value={selectedSalesperson} 
            onChange={(e) => setSelectedSalesperson(e.target.value)} 
            required 
            name="salespeople" 
            id="salespeople">
        <option value="">Choose a Salesperson</option>
        {salespeople.map(salesperson => {
        return (
            <option key={salesperson.employee_id} value={salesperson.employee_id}>
            {salesperson.first_name + ' ' + salesperson.last_name}
            </option>
        );
        })}
</select>

          <table className="table mt-4">
            <thead>
            <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
                </tr>
            </thead>
            <tbody>
              {sales.filter((sale) => {
                return sale.salesperson.employee_id === selectedSalesperson;
              }).map(sale => {
                return (
                  <tr key={sale.id}>
                    <td>{sale.salesperson.first_name}  {sale.salesperson.last_name}</td>
                    <td>{sale.customer.first_name} {sale.customer.last_name} </td>
                    <td>{ sale.automobile.vin }</td>
                    <td>{ `$${sale.price}` }</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default SaleHistory;